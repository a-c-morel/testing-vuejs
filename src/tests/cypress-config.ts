import { defineConfig } from 'cypress';

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:4173',
    specPattern: 'src/tests/**/*.cy.ts',
    fixturesFolder: 'src/tests/fixtures',
    screenshotsFolder: 'target/cypress-screenshots',
    supportFile: false,
    video: false,
    defaultCommandTimeout: 6000,
  },
});
