import App from '../../main/primary/App.vue'
import { interceptForever } from '../utils/Interceptor'

describe('<App />', () => {

  it('renders', () => {
    cy.mount(App)
  })

  it('should statically display the name of a beer', () => {
    cy.mount(App)
    cy.get("h2").contains("My Fabulous Beer")
  })

  it('should dynamically display the name of a beer', () => {
    const response = interceptForever({ path: '/api/v2/beers' }, { fixture: 'beers' });
    cy.mount(App)
    cy.get("h2").should('be.visible').then(() => {
      response.send()
      cy.get("h2").contains("Samuel Smith’s Imperial IPA")
    })
  })

})