import axios from 'axios';
import { provide } from '../injections';
import { BEERS } from './BreweryKeys';
import { RestBeersRepository } from './secondary/RestBeersRepository';

export const breweryProvider = () => {
  const axiosOpenLibrary = axios.create({
    baseURL: "https://random-data-api.com",
  });
  provide(BEERS, new RestBeersRepository(axiosOpenLibrary));
}