import axios, { type AxiosResponse, type AxiosInstance } from 'axios';
import { type BeersRepository } from '../domain/BeersRepository';
import { type Beers } from '../domain/Beers';

//This is the implementation of BeersRepository.
export class RestBeersRepository implements BeersRepository {

  constructor(private readonly axiosInstance: AxiosInstance) {}

  async get(): Promise<Beers> {
    return this.axiosInstance.get<RestBeers>('/api/v2/beers').then(toBeers);
  }
}

interface RestBeers {
    id: number;
    uid: string;
    brand: string;
    name: string;
    style: string;
    hop: string;
    yeast: string;
    malts: string;
    ibu: string;
    alcohol: string;
    blg: string;
}

const toBeers = (response: AxiosResponse<RestBeers>): Beers => {
  const data = response.data;

  return {
    id: data.id,
    uid: data.uid,
    brand: data.brand,
    name: data.name,
    style: data.style,
    hop: data.hop,
    yeast: data.yeast,
    malts: data.malts,
    ibu: data.ibu,
    alcohol: data.alcohol,
    blg: data.blg,
  };
};