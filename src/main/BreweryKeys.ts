import { key } from "piqure";
import { type BeersRepository } from './domain/BeersRepository';

export const BEERS = key<BeersRepository>('Beers');