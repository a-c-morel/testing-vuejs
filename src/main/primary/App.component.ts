import { defineComponent, onMounted, ref } from 'vue';
import { inject } from '../../injections';
import { BEERS } from '../BreweryKeys';
import { breweryProvider } from '../BreweryProvider';

breweryProvider()

export default defineComponent({
  name: 'App',
  
  setup() {
    const beers = inject(BEERS);
    const beer = ref("My Fabulous Beer")

    onMounted(() => {
      beers.get().then((response) => {
        beer.value = response.name
      })
    });

    return { beer };
  }
});