import { type Beers } from './Beers';

//This is a port. I repeat. THIS IS A PORT.
export interface BeersRepository {
    get(): Promise<Beers>;
}